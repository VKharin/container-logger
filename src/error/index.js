const logger = require('../config/logger')

class ErrorHandler {
  static processWeb (handler) {
    return async (req, res, next) => {
      try {
        await handler(req, res)
      } catch (error) {
        res.status(500).send(error)
        next(error)
      }
    }
  };

  static process (handler) {
    return async () => {
      try {
        return await handler()
      } catch (e) {
        logger.error(e.message)
      }
    }
  }
}
module.exports = ErrorHandler
