const Container = require('./containers.model')

module.exports = class ContainersService {
  async findAll () {
    return Container.find({})
  }

  async findAllEnabledContainers () {
    return Container.find({ isLoggingEnabled: true })
  }

  async setLogging (id, isEnabled = true) {
    return Container.updateOne({ _id: id }, { $set: { isLoggingEnabled: isEnabled } })
  }

  async findOne (id) {
    return Container.findOne({ _id: id })
  }

  async save (container) {
    const newContainer = new Container(container)
    return newContainer.save()
  }
}
