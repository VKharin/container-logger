module.exports = class ContainersService {
  constructor (storage) {
    this._storage = storage
  }

  async save (contianer) {
    return this._storage.save(contianer)
  }

  async findAll () {
    return this._storage.findAll()
  }

  async findOne (id) {
    return this._storage.findOne(id)
  }

  async setLogging (id, isEnabled) {
    return this._storage.setLogging(id, isEnabled)
  }
}
