const express = require('express')
const router = express.Router()
const containersController = require('./containers.controller')
const ErrorHandler = require('../../error')

router.get('/containers', ErrorHandler.processWeb(containersController.getContainers))
router.get('/setLogging/:id', ErrorHandler.processWeb(containersController.setLogging))

module.exports = router
