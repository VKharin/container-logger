const ContainersService = require('./containers.service.factory')

module.exports = class ContainersController {
  static async getContainers (req, res) {
    return res.send(await ContainersService.findAll())
  }

  static async setLogging (req, res) {
    return res.send(await ContainersService.setLogging(req.params.id, req.query.isEnabled))
  }
}
