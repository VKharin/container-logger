const mongoose = require('../../db/connection')

const { Schema } = mongoose

const Container = new Schema({
  _id: { type: String },
  image: { type: String },
  isLoggingEnabled: { type: Boolean }
}, { versionKey: false })
module.exports = mongoose.model('containers', Container)
