const LogService = require('./logs.service.factory')

module.exports = class LogController {
  static async getLogs (req, res) {
    return res.send(await LogService.findLogs())
  }

  static async createLog (req, res) {
    return res.send(await LogService.save(req.body))
  };

  static async getLogsByProgramId (req, res) {
    return res.send(await LogService.findLogs(req.params.programId))
  };
}
