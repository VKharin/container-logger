const mongoose = require('../../db/connection')

const { Schema } = mongoose

const Log = new Schema({
  programId: { type: String },
  data: { type: String }
}, { timestamps: { createdAt: true, updatedAt: false }, versionKey: false })

module.exports = mongoose.model('logs', Log)
