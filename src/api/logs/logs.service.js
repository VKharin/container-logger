module.exports = class LogService {
  constructor (storage) {
    this._storage = storage
  }

  async save (log) {
    return this._storage.save(log)
  }

  async findLogsById (id) {
    return this._storage.findAllById(id)
  }

  async findLogs () {
    return this._storage.findAll()
  }
}
