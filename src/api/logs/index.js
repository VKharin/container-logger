const express = require('express')
const logsController = require('./logs.controller')
const ErrorHandler = require('../../error')

const router = express.Router()

router.get('/logs', ErrorHandler.processWeb(logsController.getLogs))

router.get('/logs/:programId', ErrorHandler.processWeb(logsController.getLogsByProgramId))

module.exports = router
