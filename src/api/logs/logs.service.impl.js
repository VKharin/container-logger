const Log = require('./logs.model')

module.exports = class LogService {
  async findOneByProgramId (programId, offset, limit) {
    return Log.find({ programId: programId }).sort({ createdAt: -1 }).skip(offset).limit(limit)
  }

  async findAllById (programId) {
    return Log.find({ programId: programId }).sort({ createdAt: -1 })
  }

  async findAll () {
    return Log.find()
  }

  async save (log) {
    const newLog = new Log(log)
    return newLog.save()
  }

  async count (programId) {
    return Log.count({ programId: programId })
  }
}
