module.exports = {
  mongo: { url: 'mongodb://db:27017/docker' },
  port: 3000,
  dockerSocket: '/var/run/docker.sock'
}
