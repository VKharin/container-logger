const Docker = require('dockerode')
const { dockerSocket } = require('../config')

module.exports = new Docker({ socketPath: dockerSocket })
