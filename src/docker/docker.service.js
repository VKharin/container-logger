const stream = require('stream')
const docker = require('./docker')
const logger = require('../config/logger')
const ContainersService = require('../api/containers/containers.service.factory')
const LogService = require('../api/logs/logs.service.factory')
const containersInProcess = new Map()

module.exports = class DockerService {
  static async saveContainersInfo () {
    const containers = await new Promise((resolve, reject) => {
      docker.listContainers(function (err, containers) {
        if (err) {
          reject(err)
        } else {
          resolve(containers)
        }
      })
    })
    for (let i = 0; i < containers.length; i++) {
      const containerFromDb = await ContainersService.findOne(containers[i].Id)
      if (containerFromDb === null) {
        await ContainersService.save({ _id: containers[i].Id, image: containers[i].Image, isLoggingEnabled: false })
      }
    }
  }

  static async saveLogs () {
    const containers = await ContainersService.findAll()
    for (let i = 0; i < containers.length; i++) {
      if (!containersInProcess.has(containers[i].id) && containers[i].isLoggingEnabled) {
        logger.info('start loging for ' + containers[i].id)
        const logStream = DockerService._containerLogs(containers[i].id)
        containersInProcess.set(containers[i].id, logStream)
      } else if (containersInProcess.has(containers[i].id) && !containers[i].isLoggingEnabled) {
        logger.info('destroy loging for ' + containers[i].id)
        const logStream = containersInProcess.get(containers[i].id)
        logStream.destroy()
        containersInProcess.delete(containers[i].id)
      }
    }
  }

  static _containerLogs (id) {
    const container = docker.getContainer(id)
    const logStream = new stream.PassThrough()
    logStream.on('data', function (chunk) {
      const data = chunk.toString('utf8')
      LogService.save({ programId: id, data: data })
    })
    container.logs({
      follow: true,
      stdout: true,
      stderr: true
    }, (err, stream) => {
      if (err) {
        return err
      }
      container.modem.demuxStream(stream, logStream, logStream)
      stream.on('end', (logStream, id) => DockerService._endStream)
    })
    return logStream
  }

  static _endStream (logStream, id) {
    logStream.end('!stop!')
    containersInProcess.delete(id)
  }
}
