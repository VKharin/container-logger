const mongoose = require('mongoose')
const { mongo } = require('../config')
const logger = require('../config/logger')

mongoose.connect(mongo.url, { useNewUrlParser: true, useUnifiedTopology: true }).then(() => logger.info('Connection ok!')).catch(err => logger.error(err))

module.exports = mongoose
