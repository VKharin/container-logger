const CronJob = require('cron').CronJob
const ErrorHandler = require('../error')
const DockerService = require('../docker/docker.service')

module.exports = class Cron {
  static updateContainerInfo () {
    Cron._startJob('*/30 * * * * *', ErrorHandler.process(async () =>
      DockerService.saveContainersInfo()
    ))
  }

  static saveLogs () {
    Cron._startJob('*/10 * * * * *', ErrorHandler.process(async () => DockerService.saveLogs()))
  }

  static _startJob (pattern, job) {
    const cronJob = new CronJob(pattern, job)
    cronJob.start()
  }
}
