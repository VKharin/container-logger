To start the application, you first need to install docker and docker-compose.
Then you need to run command docker-compose -f docker-compose.yml up -d from the command line in directory of application.
The application reads Docker containers every 30 seconds and stores them in a database.
To get available containers you must call localhost:3000/containers
To start receiving logs from containers, you must enable logging for the container.
To enable logging for the container you must call localhost:3000/setLogging/:id?isEnabled=true, where id is id of docker container.
To get saved logs for the container you must call localhost:3000/logs/:id, where id is id of docker container.

API
localhost:3000/containers - show docker container list
localhost:3000/setLogging/:id?isEnabled=true - enable or disable logging by containerId
localhost:3000/logs - show all logs saved in db
localhost:3000/logs/:id - show logs by containerId
